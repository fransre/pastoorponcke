all: pastoorponcke.pdf

pastoorponcke.epub: epub/mimetype epub/META-INF/* epub/OEBPS/*
	cd epub
	zip -X --quiet -0 ../pastoorponcke.epub mimetype
	zip -X --quiet -9 --recurse-paths --no-dir-entries ../pastoorponcke.epub * --exclude '**/.DS_Store'
	cd ..
	epubcheck pastoorponcke.epub

pastoorponcke.pdf: pastoorponcke.epub
	ebook-convert pastoorponcke.epub pastoorponcke.pdf

fontawesome-webfont-minimaal.woff2: fontawesome-webfont.woff2
	pyftsubset fontawesome-webfont.woff2 --unicodes="U+F016,U+F02D,U+F040,U+F067,U+F08B,U+F090,U+F097,U+F0CB,U+F0F6,U+F137,U+F138,U+F15D,U+F24E,U+F266,U+F27B,U+F2C0" --layout-features="" --flavor="woff2" --output-file="fontawesome-webfont-minimaal.woff2"
